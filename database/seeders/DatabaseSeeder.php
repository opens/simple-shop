<?php

namespace Database\Seeders;

use App\Models\Price;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Product::factory(5)->create()->each(function ($product) {
            Price::factory()->count(3)->create(['product_id' => $product->id]);
        });
        User::insertOrIgnore([
            'name' => 'test',
            'email' => 'test@example.com',
            'password' => Hash::make('test'),
        ]);
    }
}
