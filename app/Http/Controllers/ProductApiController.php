<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductApiController extends Controller
{
    public function index(Request $request)
    {
        $query = Product::query();

        $sortFields = $request->get('sort', []);
        $sortDirections = $request->get('sort_dir', []);

        foreach ($sortFields as $key => $sortField) {
            $sortDirection = $sortDirections[$key] ?? 'asc';
            $query->orderBy($sortField, $sortDirection);
        }

        if ($request->has('search')) {
            $searchQuery = $this->parseSearchQuery($request->get('search'));
            $searchTerm = $searchQuery['term'];
            $pageNumber = $searchQuery['page'];

            $query->where(function ($query) use ($searchTerm) {
                $query->where('id', 'like', "%$searchTerm%")
                    ->orWhere('name', 'like', "%$searchTerm%")
                    ->orWhere('category', 'like', "%$searchTerm%")
                    ->orWhere('description', 'like', "%$searchTerm%")
                    ->orWhere('created_at', 'like', "%$searchTerm%")
                    ->orWhere('updated_at', 'like', "%$searchTerm%");
            });
        }
        if ($request->has('page')) {
            $pageNumber = $request->input('page');
        }
        $products = $query->paginate(10, ['*'], 'page', $pageNumber ?? 1);

        return ProductResource::collection($products);
    }

    private function parseSearchQuery($searchQuery)
    {
        $matches = [];
        preg_match('/^(.*)\?page=(\d+)$/', $searchQuery, $matches);

        return [
            'term' => $matches[1] ?? $searchQuery,
            'page' => $matches[2] ?? 1
        ];
    }

    public function show($id)
    {
        $product = Product::with('prices')->findOrFail($id);
        return new ProductResource($product);
    }

    public function store(ProductRequest $request)
    {
        $product = Product::create($request->validated());
        return new ProductResource($product);
    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->validated());
        return new ProductResource($product);
    }

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return response()->noContent();
    }
}
