<?php

namespace Tests\Feature;

use App\Models\Product;
use Database\Seeders\DatabaseSeeder;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTruncation;
use Tests\TestCase;

class ProductControllerTest extends TestCase
{
    use DatabaseTruncation;
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(DatabaseSeeder::class);
    }

    public function testCanListProducts()
    {
        $response = $this->getJson('/api/products');

        $response->assertOk();

        $response->assertJsonCount(5, 'data');
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'category',
                    'description',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);
    }

    public function testCanSortProducts()
    {
        $response = $this->getJson('/api/products?sort[]=name&sort_dir[]=asc');

        $response->assertOk();

        $productNames = Product::orderBy('name')->get()->pluck('name')->toArray();
        $responseNames = collect($response->json('data'))->pluck('name')->toArray();
        $this->assertEquals($productNames, $responseNames);
    }

    public function testCanFilterProductsByCategory()
    {
        $product = Product::first();
        $response = $this->getJson("/api/products?search=" . $product->category);

        $response->assertOk();

        $responseCategories = collect($response->json('data'))->count();
        $count = Product::where('category', $product->category)->get()->count();

        $this->assertEquals($count, $responseCategories);
    }

    public function testCanCreateProduct()
    {
        $data = [
            'name' => fake()->name,
            'category' => 'Electronics',
            'description' => fake()->sentence,
        ];

        $response = $this->postJson('/api/products', $data);

        $response->assertCreated();

        $this->assertDatabaseHas('products', $data);

        $response->assertJson([
            'data' => [
                'name' => $data['name'],
                'category' => $data['category'],
                'description' => $data['description'],
            ],
        ]);
    }

    public function testCanUpdateProduct()
    {
        $product = Product::factory()->create();

        $data = [
            'name' => fake()->name,
            'category' => 'Clothing',
            'description' => fake()->sentence,
        ];

        $response = $this->putJson("/api/products/{$product->id}", $data);

        $response->assertOk();

        $this->assertDatabaseHas('products', $data);

        $response->assertJson([
            'data' => [
                'name' => $data['name'],
                'category' => $data['category'],
                'description' => $data['description'],
            ],
        ]);
    }

    public function testCanDeleteProduct()
    {
        $product = Product::factory()->create();

        $response = $this->deleteJson("/api/products/{$product->id}");

        $response->assertNoContent();

        $this->assertDatabaseMissing('products', ['id' => $product->id]);
    }
}
