@extends('layouts.app')

@section('content')
    <div id="wrapper"></div>
    <script>

        new gridjs.Grid({
            pagination: {
                limit: 10,
                server: {
                    url: (prev, page) => `${prev}?page=${page + 1}`
                }
            },
            search: {
                server: {
                    url: (prev, keyword) => `${prev}?search=${keyword}`,
                },
            },
            columns: ['id', 'name', 'category', 'description', 'created_at', 'updated_at', 'actions'],
            sort: true,
            server: {
                url: '{{url('api/products')}}',
                then: data => data.data.map(movie => [
                    [
                        gridjs.html(`<a target="_blank" href='{{url('products/')}}/${movie.id}'>${movie.id}</a>`)
                    ],
                    [
                        gridjs.html(`<a target="_blank" href='{{url('products/')}}/${movie.id}'>${movie.name}</a>`)
                    ],
                    movie.category, movie.description, movie.created_at, movie.updated_at,
                    [
                        @if(Auth::check())
                        gridjs.html(`<a href='{{url('products/')}}/${movie.id}/edit' class='btn btn-primary'>Edytuj</a>
                        <a href='{{url('products/')}}/${movie.id}' class='btn btn-danger' onclick='confirmDelete(event, ${movie.id})'>Usuń</a>`)
                        @endif
                    ],
                ]),
                total: data => data.meta.total
            },
        }).render(document.getElementById("wrapper"));

        function confirmDelete(event, productId) {
            event.preventDefault();
            if (confirm('Czy na pewno chcesz usunąć ten produkt?')) {
                deleteProduct(productId);
            }
        }

        function deleteProduct(productId) {
            let csrfToken = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '{{url('api/products')}}/' + productId,
                headers: {
                    'X-CSRF-TOKEN': csrfToken
                },
                method: 'DELETE',
                success: function (response) {
                    alert('Produkt został usunięty.');
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }
    </script>
@endsection
