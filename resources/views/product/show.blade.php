@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="mb-4">Szczegóły produktu</h1>
        <div id="product-details">
        </div>
    </div>
    <script>
        $(document).ready(function () {
            let productId = window.location.href.split('/').pop();
            $.ajax({
                url: '{{url('/api/products/')}}/' + productId,
                method: 'GET',
                success: function (response) {
                    let product = response.data;

                    let html = '<h2>' + product.name + '</h2>';
                    html += '<p class="mb-3">Kategoria: ' + product.category + '</p>';
                    html += '<p>Opis: ' + product.description + '</p>';
                    html += '<h3 class="mt-4">Ceny:</h3>';
                    html += '<ul class="list-group">';

                    for (let i = 0; i < product.prices.length; i++) {
                        let price = product.prices[i].price;
                        html += '<li class="list-group-item">' + price + ' zł</li>';
                    }

                    html += '</ul>';

                    $('#product-details').html(html);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        });
    </script>
@endsection
