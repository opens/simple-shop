@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="mb-4">Edycja produktu</h1>
        <div id="product-details">
            <form id="edit-product-form" method="POST">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="name" class="form-label">Nazwa</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="mb-3">
                    <label for="category" class="form-label">Kategoria</label>
                    <input type="text" class="form-control" id="category" name="category">
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Opis</label>
                    <textarea class="form-control" id="description" name="description"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Zapisz</button>
            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            let productId = window.location.href.split('/').slice(-2, -1)[0];
            let editProductForm = document.getElementById('edit-product-form');
            $.ajax({
                url: '{{url('/api/products/')}}/' + productId,
                method: 'GET',
                success: function (response) {
                    let product = response.data;
                    $('#name').val(product.name);
                    $('#category').val(product.category);
                    $('#description').val(product.description);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });

            editProductForm.addEventListener('submit', function (event) {
                event.preventDefault();

                let name = $('#name').val();
                let category = $('#category').val();
                let description = $('#description').val();

                $.ajax({
                    url: '{{url('/api/products/')}}/' + productId,
                    method: 'PUT',
                    data: {
                        name: name,
                        category: category,
                        description: description
                    },
                    success: function (response) {
                        alert('Produkt został zaktualizowany.');
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
            });
        });
    </script>
@endsection
